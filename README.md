# layui-formTable

## 介绍

-----
form input搜索以表格提示内容

## 说明

版本要求layui2.7+

## 代码示例

-----

```JS
<input type="text" name="parts" autocomplete="off" class="layui-input">

layui.config({
  base: '/plugin/layui/common/', //拓展模块的根目录
})
  .extend({
    formTable: '/formTable/formTable'
  });
layui.use(['table', 'form', 'formTable'], function () {
  const formTable= layui.formTable;
  // 查询自动提示
  const searchTable = formTable.render({
    elem: 'input[name=parts]',
    url: "/getPart",
    searchKey: 'car_number', // 设置搜索
    searchKeyMinLength: 4, // 设置搜索最少字符长度
    width: 730, page: true, limit: 15,   // 分页
    table: {
      cols: [[
        { type: 'numbers', title: '编码', width: '3%' },
        { field: 'part_brand', title: '品牌', width: '10%' },
      ]],
      height: 180,
      tool: {
        'add': (data) => {
          // 表格tool触发事件
        },
      },
      rowDouble: (data) => {
        //触发行双击事件
      }
    },
  })

  // 关闭弹框
  searchTable.hide()
  // 重置实例
  searchTable.reload()
})
```

## 展示案例

<!-- 图片 -->
![这是图片](./image.png "展示案例")

## layui扩展功能

[一个基于layui的自定义表单](https://gitee.com/Mr.zhaoGW/layui-diy-form)

<!-- 图片 -->
![这是图片](layui-diy-form.gif "展示案例")


[一个综合表单条件过滤器](https://gitee.com/Mr.zhaoGW/layui-filter-container)

<!-- 图片 -->
![这是图片](layui-filter-container.gif "展示案例")
