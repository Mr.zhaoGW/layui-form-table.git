layui.define(['table', 'laytpl'], function (exports) {
  "use strict";

  var $ = layui.$
    , table = layui.table
    , laytpl = layui.laytpl
    , hint = layui.hint()

    //模块名
    , MOD_NAME = 'formTable'
    , MOD_INDEX = 'layui_'+ MOD_NAME +'_index' //模块索引名

    //外部接口
    , modeDemo = {
      config: {}
      , index: layui[MOD_NAME] ? (layui[MOD_NAME].index + 10000) : 0

      //设置全局项
      , set: function (options) {
        var that = this;
        that.config = $.extend({}, that.config, options);
        return that;
      }

      //事件监听
      , on: function (events, callback) {
        return layui.onevent.call(this, MOD_NAME, events, callback);
      }
    }

    //操作当前实例
    , thisModule = function () {
      var that = this
        , options = that.config
        , id = options.id || that.index;

      thisModule.that[id] = that; //记录当前实例对象
      thisModule.config[id] = options; //记录当前实例配置项

      return {
        config: options
        //重置实例
        , reload: function (options) {
          that.reload.call(that, options);
        },
        hide: function (options) {
          that.hide.call(that, options);
        },
        show: function (options) {
          that.show.call(that, options);
        }
      }
    }
    // 请求节流
    , throttle = function (func, delay) {
			let timerId;
			return function(...args) {
				if (!timerId) {
					timerId = setTimeout(() => {
						func.apply(this, args);
						timerId = null;
					}, delay);
				}
			};
		}

    //获取当前实例配置项
    , getThisModuleConfig = function (id) {
      var config = thisModule.config[id];
      if (!config) hint.error('The ID option was not found in the ' + MOD_NAME + ' instance');
      return config || null;
    }

    //字符常量  
    , ELEM = 'layui-modeDemo'


    //主模板
    , TPL_MAIN = `<div class="${MOD_INDEX}-{{= d.index }} layui-anim layui-anim-upbit" 
      style="background-color: #fff;padding:10px;position: absolute;z-index:1000;margin: 5px 0; 
      border: 1px solid #f2f2f2; box-shadow: 0 0 5px 2px rgba(0,0,0,.1);display: none;">
    </div>`

    //构造器
    , Class = function (options) {
      var that = this;
      that.index = ++modeDemo.index;
      that.config = $.extend({}, that.config, modeDemo.config, options);
      that.render();
    };

  //默认配置
  Class.prototype.config = {
    searchKeyMinLength: 0, //搜索关键字最小长度
  };

  //重载实例
  Class.prototype.reload = function (options) {
    var that = this;

    layui.each(options, function (key, item) {
      if (item.constructor === Array) delete that.config[key];
    });

    that.config = $.extend(true, {}, that.config, options);
    that.render();
  };

  //渲染
  Class.prototype.render = function () {
    var that = this
      , options = that.config;

    //解析模板
    // that.elem = $(options.elem);

    var othis = options.elem = $(options.elem);
    if (!othis[0]) return;

    //索引
    options.id = options.id || that.index;

    //插入组件结构
    // othis.html(that.elem);

    that.events(); //事件
  };

  //事件
  Class.prototype.events = function () {
    var that = this,
      a = that.config;
    a.elem.on("click", (e) => {
      that.createTable();
    })
    let isComposing = false;
    // 监听输入框的 compositionstart 事件，表示开始输入中文
    a.elem.on('compositionstart', function() {
      isComposing = true;
    });
    // 监听输入框的 compositionend 事件，表示结束输入中文
    a.elem.on('compositionend', function() {
      isComposing = false;
    })
    a.elem.on("input", throttle((e) => {
      if (isComposing) return;
      that.pullData();
    }, 500))
  };
  Class.prototype.createTable = function () {
    var that = this,
        a = that.config;
    // 判断是否已经创建过, 如果已经创建过则不再创建
    if ($(`.${MOD_INDEX}-${that.index}`).length >= 1) {
      return false;
    }
    // 判断是否有值, 如果没有值则不创建
    if (!a.elem.val()) {
      delete table.cache[tableName];
    }
    var tableDone = a.table.done || function () {};
    var t = a.elem.offset().top + a.elem.outerHeight() + "px";
    var l = a.elem.offset().left + "px";
    var w = a.width ? `${a.width}px` : '';
    var h = a.height ? `${a.height}px` : '';
    var tableName = `${MOD_NAME}_table_`.concat(a.id);
    var tableTpl = `<table id="${tableName}" lay-filter="${tableName}"></table>`;
    // 解析模板
    var main = that.elem = $(laytpl(TPL_MAIN).render({
      data: a
      ,index: that.index //索引
    }));
    main.css({'left': l, 'top': t, 'width': w, 'height': h})
    main.append(tableTpl);
    $("body").append(main);

    //渲染TABLE
    a.table.elem = "#" + tableName;
    a.table.id = tableName;
    a.table.data = table.cache[tableName] || {};
    a.table.page = a.page || false;
    a.table.limit = a.limit
    
    a.table.done = function (res, curr, count) {
      // defaultChecked(res, curr, count);
      // setChecked(res, curr, count);
      // tableDone(res, curr, count);
    };

    a.table.body = table.render(a.table);

    that.config = $.extend(true, {}, that.config, a);

    //触发事件
    table.on(`tool(${tableName})`, function(obj){
      var data = obj.data;
      var layEvent = obj.event;
      a.table.tool[layEvent](data)
    });

    //触发行双击事件
    table.on(`rowDouble(${tableName})`, function(obj){
      var data = obj.data;
      // 回调
      typeof a.table.rowDouble === 'function' && a.table.rowDouble(data, obj);
    });

    // 单选框事件
    table.on(`radio(${tableName})`, function(obj){
      // 回调
      typeof a.table.radio === 'function' && a.table.radio(obj);
    });

    //点击其他区域关闭
    $(document).mouseup(function (event) {
      if (!main.is(event.target) && main.has(event.target).length === 0) {
        that.hide();
      }
    });
    
  }

  Class.prototype.pullData = function () {
    var i = this,
      a = i.config,
      h = a.table;
    i.startTime = new Date().getTime(); // 渲染开始时间
    if (a.url) {
      const val = $(a.elem).val();
      if (!val.length || a.searchKeyMinLength > val.length) { return false; }
      const field = a.searchKey || 'val';
      var where = { [field]: val};
      if (h.body != undefined) {
        table.reloadData(h.body.config.id, {
          url: a.url,
          where: where
        });
      } else {
        i.createTable();
      }
    }
  }
  Class.prototype.selectTool = function () {
    
  }
  // 显示
  Class.prototype.show = function () {
    var that = this,
        a = that.config;
    $(`.${MOD_INDEX}-${that.index}`).css({'display': ''})
  };
  // 隐藏
  Class.prototype.hide = function () {
    var that = this,
        a = that.config;
    $(`.${MOD_INDEX}-${that.index}`).css({'display': 'none'})
    delete table.cache[a.table.id];
  };
  //记录所有实例
  thisModule.that = {}; //记录所有实例对象
  thisModule.config = {}; //记录所有实例配置项

  //重载实例
  modeDemo.reload = function (id, options) {
    var that = thisModule.that[id];
    that.reload(options);
    return thisModule.call(that);
  };
  //核心入口
  modeDemo.render = function (options) {
    var inst = new Class(options);
    return thisModule.call(inst);
  };
  
  exports(MOD_NAME, modeDemo);
});
